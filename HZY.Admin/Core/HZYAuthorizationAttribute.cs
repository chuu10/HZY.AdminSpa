﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HZY.Admin.Core
{
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Filters;
    using System.Net;
    using System.Security.Claims;
    using Toolkit;

    public class HZYAuthorizationAttribute : ActionFilterAttribute
    {
        /// <summary>
        /// 忽略本特性
        /// </summary>
        public bool Ignore { get; set; } = false;

        /// <summary>
        /// 每次请求Action之前发生，，在行为方法执行前执行
        /// </summary>
        /// <param name="context"></param>
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            base.OnActionExecuting(context);

            if (Ignore) return;

            if (!context.HttpContext.Request.Headers.ContainsKey("Authorization"))
                throw new MessageBox(StatusCodeEnum.未授权, $"{StatusCodeEnum.未授权.ToString()}");

            var _ClaimsIdentity = context.HttpContext.User.Identity as ClaimsIdentity;
            var _Id = _ClaimsIdentity.Name;

            if (_Id.ToGuid() == Guid.Empty)
                throw new MessageBox(StatusCodeEnum.未授权, $"{StatusCodeEnum.未授权.ToString()}");
        }




    }
}
