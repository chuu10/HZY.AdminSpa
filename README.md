# HZY.AdminSpa

#### 捐赠 大哥们，如果觉得还行就请我喝瓶水吧
|  ![输入图片说明](https://images.gitee.com/uploads/images/2020/0302/162422_94ee4116_1242080.jpeg "微信图片_20200302162149.jpg")   |  ![输入图片说明](https://images.gitee.com/uploads/images/2020/0302/162431_7e7da4ea_1242080.jpeg "微信图片_20200302162144.jpg")   |
| --- | --- |



#### 介绍

 后台管理系统、通用权限、单页面（VUE） **交流群: 534584927** 

 脚本文件：hzyadminantdv.sql

#### 软件架构

前后端分离框架

后端：.Net Core 3.1 、Swagger

前端：Vue 全家桶 、[Ant Design of Vue](https://www.antdv.com/docs/vue/introduce-cn/) 、CKEditor5 Vue

#### 安装教程

1、安装 nodejs

2、安装 vue cli >> cmd 执行: npm install -g @vue/cli

#### 安装教程

1. 找到目录 HzyAdminSpa/ Admin / ClientApp 使用 VS Code 打开
2. VS Code 打开终端执行CMD命令>> cnpm install 拉包 （node 环境 这些不懂得自行百度查询资料！）
3. 然后使用 Vs 2019 打开服务端代码 f5 调试模式 运行即可
注意：
![输入图片说明](https://images.gitee.com/uploads/images/2019/1224/131124_8c2c3463_1242080.png "屏幕截图.png")

请使用这种独立模式启动，不要使用 iis 模式

4. 地址栏键入 /swagger 进入前端接口文档页面
5. 地址栏键入 /cct 进入代码创建页面



 **代码创建/下载（登录系统后，输入地址 /cct 即可进入代码创建页面）：** 

![输入图片说明](https://images.gitee.com/uploads/images/2019/1026/142626_778c5ce5_1242080.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/1026/142717_66e4c7a0_1242080.png "屏幕截图.png")

 **Swagger文档（输入地址 /swagger 即可进入接口文档页面）：** 

![输入图片说明](https://images.gitee.com/uploads/images/2019/1025/192706_27cde59b_1242080.png "屏幕截图.png")

 **系统内部页面：** 

![登录](https://images.gitee.com/uploads/images/2020/0210/143947_0da5fc5b_1242080.png "屏幕截图.png")


![首页](https://images.gitee.com/uploads/images/2020/0210/144042_e1ae731a_1242080.png "屏幕截图.png")


![换肤](https://images.gitee.com/uploads/images/2020/0210/144448_b66ffe29_1242080.png "屏幕截图.png")


![帐户编辑](https://images.gitee.com/uploads/images/2020/0210/144122_31b511fc_1242080.png "屏幕截图.png")


![菜单功能](https://images.gitee.com/uploads/images/2020/0210/144151_18f4f68f_1242080.png "屏幕截图.png")


![角色功能](https://images.gitee.com/uploads/images/2020/0210/144230_64933884_1242080.png "屏幕截图.png")

#### 使用说明

如果部署iis访问不了的情况可以用两种办法：

1、直接打开exe然后控制台看错误

2、web.config里面有个false 改为 true，iis重启项目后运行网站后，跟目录下面 有个文件夹 log 里面有错误日志文件

3、一定注意 项目 bin/netcoreapp3.1/debug/app.xml 放入部署项目的跟目录中 

4、记得前端 vue 页面 打包

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)